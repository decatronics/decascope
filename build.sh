echo "Cleaning..."
rm -rf dist 2> /dev/null

echo "Building..."
pyminifier -o decascope_min.py --lzma decascope_qtgui.py > /dev/null
cat decascope_min.py | head -n 3 > decascope.py

echo "Copying files..."
mkdir dist
mv decascope.py dist
rm decascope_min.py
cp README.md dist
cp -r resources dist

mkdir dist/firmware
cp Decascope.cpp dist/firmware
cp Decascope.h dist/firmware
echo "Done."

echo "MANUAL: Update versions in decascope_qtgui.py, C, README, setup.py and website"