/* 

Decascope

Simple two-channel oscilloscope designed for Arduino.
See https://decatronics.gitlab.io/decascope for more information.

Author: Jason Storey
*/
#include "decascope.h"

void init_adc(void);

void setup() {
	// Set IO mode
	pinMode(CHANNEL_1, INPUT);
	pinMode(CHANNEL_2, INPUT);
	pinMode(PWM_OUT, OUTPUT);

	// Test signal
	analogWrite(PWM_OUT, 50);

	// Get serial ready and print "ready" message
	Serial.begin(BAUDRATE);

	// Get ADC ready for quicker conversions
	init_adc();

	// Print version message. (Required for detection)
	Serial.println(String("Decascope v") + String(VERSION_MAJ) + String(".") + String(VERSION_MIN));
}

void loop() {
	// Wait for signal that buffer is ready to send
	if (start_transmit) { 

		if (shadow_buffers) {
			send_buffer_blocks(channel1_buffer, channel2_buffer)
		} else {
			send_buffer_blocks(channel1_shadow, channel2_shadow)
		}

		start_transmit = false; // Reset signal
	}
}

/* Send a pair of buffers over serial
Arguments: buffer1 and buffer2 are the buffers to send
*/
void send_buffer_blocks(uint8_t *buffer1, uint8_t *buffer2) {
	// Channel 1 indicator
	Serial.write(CHANNEL_1_INDEX);

	// Actual buffer
	Serial.write((uint8_t *)buffer1, BLOCK_SIZE);

	// Terminator
	Serial.write(0); // Terminator

	// Channel 2 indicator, etc
	Serial.write(CHANNEL_2_INDEX);
	Serial.write((uint8_t *)buffer2, BLOCK_SIZE);
	Serial.write(0);
}

void init_adc() {
	// Disable ADC
	cbi(ADCSRA, ADEN);

	// Disable digital input buffering on input channels
	sbi(DIDR0,ADC1D);
	sbi(DIDR0,ADC0D);

	// Left-adjust so the result can be read immediately as 8-bit
	sbi(ADMUX,ADLAR);

	// Use AVCC
	sbi(ADMUX,REFS0);
	cbi(ADMUX,REFS1);

	cbi(ADCSRA,ADPS0);
	sbi(ADCSRA,ADPS1);
	sbi(ADCSRA,ADPS2);

	// Start reading from A0 always
	cbi(ADMUX, MUX0);
	cbi(ADMUX, MUX1);
	cbi(ADMUX, MUX2);
	cbi(ADMUX, MUX3);

	// Enable conversion complete interrupt
	sbi(ADCSRA, ADIE);

	// Auto trigger enable
	sbi(ADCSRA, ADATE);

	// Enable ADC
	sbi(ADCSRA, ADEN);

	// Start the ADC (in free-running mode by default)
	sbi(ADCSRA, ADSC);
}

/*
	This runs every time there is an ADC result
	All it will do is store it in the appropriate buffer
*/
ISR(ADC_vect)
{
	// Inlining everything to avoid function call overhead
	uint8_t val = ADCH; // Discard ADCL

	if (val == 0) {
		val = 1; // 0 is reserved for termination
	}

	switch (reading_channel) {
		case 1:
			if (shadow_buffers) {
				channel1_shadow[channel1_head] = val;
			} else {
				channel1_buffer[channel1_head] = val;
			}

			channel1_head = (channel1_head + 1) % BLOCK_SIZE;

			if (channel1_head == 0) {
				start_transmit = true;
				// Swap shadow
				shadow_buffers = !shadow_buffers;
			} 

			sbi(ADMUX, MUX0); // Swap channels to next
			break;
		case 2:
			if (shadow_buffers) {
				channel2_shadow[channel1_head] = val;
			} else {
				channel2_buffer[channel1_head] = val;
			}

			channel2_head = (channel1_head + 1) % BLOCK_SIZE;

			cbi(ADMUX, MUX0); // Swap channels to next
			break;
	}

	// Swap which channel we're reading from
	reading_channel = (reading_channel % CHANNEL_COUNT) + 1;
}