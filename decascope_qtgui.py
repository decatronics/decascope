#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import time
import serial
import serial.tools as tools
import serial.tools.list_ports as list_ports
import numpy as np
import datetime
from itertools import cycle
from os import path

import pyqtgraph as pg
import pyqtgraph.functions as fn
import pyqtgraph.exporters
from PyQt5 import QtGui, QtCore
import atexit
import math

# Logging
import logging
logging.basicConfig(level=logging.DEBUG)

# Global config
VERSION_MAJ = 1
VERSION_MIN = 0
pg.setConfigOptions(antialias=True, background=None, foreground=(20,20,20))

class SignalChannel:
    def __init__(self, channelName, pen='b'):
        self.channelName = channelName
        
        # Stores time-voltage data
        self.channelData = ([], [])
        
        # Make curves for individual plot items
        # Full: Entire data set to show in the zoom window
        # Partial: A section of the data set, for the scope window
        self.curveSet = {
            "full": pg.PlotCurveItem(),
            "partial": pg.PlotCurveItem()
        }

        for c in self.curveSet.values():
            c.setPen(pen)

        self.mainCurve = pg.PlotCurveItem()
        self.mainCurve.setPen(pen)
        
        self.zoomCurve = pg.PlotCurveItem()
        self.zoomCurve.setPen(pen)

        self.holdBuffer = [] # For the rest of a data block when trigger is met

    def get_full_curve(self):
        return self.curveSet["full"]

    def get_zoomed_curve(self):
        return self.curveSet["partial"]

    def clear_buffers(self):
        self.holdBuffer = []

    def get_data(self):
        return self.channelData

    def get_data_bounded(self):
        """Get whatever data is currently visible"""
        return self.curveSet["partial"].getData()

    def get_channel_number(self):
        """Find the last number in the channel name (guaranteed at least one)"""
        return [int(s) for s in self.channelName.split() if s.isdigit()][-1]

    def set_data(self, data):
        self.channelData = data
        x, y = self.channelData
        self.curveSet["full"].setData(x, y)
        self.curveSet["partial"].setData(x, y)

    def set_data_bounded(self, data, start, end):
        """Data is an x,y tuple of arrays. 

        Set full curve to new data, use bounds to set data on zoomed curve
        """
        self.channelData = data
        x, y = self.channelData
        self.curveSet["full"].setData(x, y)
        self.curveSet["partial"].setData(x[start:end], y[start:end])

    def set_visible(self, visible=True):
        for curve in self.curveSet.values():
            curve.setVisible(visible)

class SigMeas():
    def __init__(self, reducer, fmt="meas({channels}) = {results}"):
        """Create a signal measurement object
        
        Parameters:
            reducer: The function that reduces a signal into a result
            fmt: A string formatting function. Optionally use "{channels}" and "{results}" for the 
            comma separated list of 1-indexed channels that were given to the function, and results corresponding
            (if any)
        """
        self.reducer = reducer
        self.fmt = fmt
        self.signals = []
        self.results = []
        pass

    def connect(self, signals):
        """Connect a sig-meas object to signal channels"""
        if isinstance(signals, list):
            self.signals = signals
        else:
            self.signals = [signals]

    def update(self):
        """Perform a calculation based on current signal data"""
        self.results = self.reducer([s.get_data() for s in self.signals])
        return self.results

    def get_results(self):
        return self.update()

    def to_string(self):
        return self.__str__()

    def __str__(self):

        channels = [str(s.get_channel_number()) for s in self.signals]
        channels = ','.join(channels)

        results = ["{:.2f}".format(r) for r in self.get_results()]
        results = ','.join(results)

        return self.fmt.format(channels=channels, results=results)

    def __eq__(self, other):
        kernel = other.reducer is self.reducer and other.fmt is self.fmt 
        signalMatch = True
        for s in self.signals:
            if not s in other.signals:
                signalMatch = False
        return kernel and signalMatch
    
    """Standard sig-meas functions.

    Parameter: signals
        An array-like object containing 2D np.arrays of each channel, in indexing order, with X and Y correspondence

    Returns: results
        An array-like object, containing a result per channel.

    """
    def peak_to_peak(signalArrays):
        """Returns the peak-to-peak amplitude of the input signalArrays"""
        return [np.max(s[1]) - np.min(s[1]) for s in signalArrays]
    
    def frequency(signalArrays):
        """Returns the estimated frequency of the input signalArrays"""
        return [SigMeas.single_freq(s) for s in signalArrays]
    
    def period(signalArrays):
        """Returns the estimated period of the input signalArrays"""
        return [1/SigMeas.single_freq(s) for s in signalArrays]
    
    def max(signalArrays):
        """Returns the maximum value of the input signalArrays"""
        return [np.max(s[1]) for s in signalArrays]
    
    def average(signalArrays):
        """Returns the average value of the input signalArrays"""
        return [np.mean(s[1]) for s in signalArrays]
    
    def min(signalArrays):
        """Returns the minimum value of the input signalArrays"""
        return [np.min(s[1]) for s in signalArrays]
    
    def phase_1_2(signalArrays):
        """Returns the phase shift of 2 input signalArrays with channel 1 leading channel 2"""
        A, B = [s[1] for s in signalArrays]

        nsamples = A.size

        # Find cross-correlation
        xcorr = correlate(A, B)

        # delta time array to match xcorr
        dt = numpy.arange(1-nsamples, nsamples)

        recovered_time_shift = dt[xcorr.argmax()]

        return [recovered_time_shift]
    
    def phase_2_1(signalArrays):
        """Returns the phase shift of 2 input signalArrays with channel 2 leading channel 1"""
        return -phase_1_2(signalArrays)

    def single_freq(signal):
        """Find the frequency of a single 1D array"""

        # Find the timescale in seconds
        scale = (signal[0][1] - signal[0][0]) / 1000

        w = np.fft.fft(signal[1])
        freqs = np.fft.fftfreq(len(w), d=scale)
        threshold = 0.5 * max(abs(w))
        mask = abs(w) > threshold
        peaks = freqs[mask]
        return peaks.max()

class ScopePlot(pg.GraphicsLayoutWidget):
    MAX_Y = 5
    MIN_Y = 0

    TIMESCALE = 110.16/1024 # milliseconds per datapoint
    MIN_TIMESCALE = 50
    DEFAULT_TIMESCALE = 100
    MAX_TIMESCALE = 5000

    MIN_DATAPOINTS = int(MIN_TIMESCALE / TIMESCALE) - 1
    DEFAULT_DATAPOINTS = int(DEFAULT_TIMESCALE / TIMESCALE) + 1
    MAX_DATAPOINTS = int(MAX_TIMESCALE / TIMESCALE) + 1

    MIN_ZOOM_WIDTH = 1 # Percent of the overall region

    triggered = QtCore.Signal(object)

    def __init__(self):
        super(ScopePlot, self).__init__()

        # Default data range is midway between max and min
        self.dataRange = ScopePlot.DEFAULT_DATAPOINTS
        self.voltageScale = ScopePlot.MIN_Y, ScopePlot.MAX_Y

        # Container for named data channels
        self.channels = {}

        # The XY curve is a derivative curve,
        # doesn't belong as part of a channel
        self.xyCurve = pg.PlotCurveItem(pen="#B024D8")
        self.xySource = None # Need a 2-tuple of channels, first is X, second is Y

        # Three plots the layout could show
        self.cursors = None
        self.plots = {
            "zoom": pg.PlotItem(),
            "window": pg.PlotItem(),
            "xy": pg.PlotItem(),
        }

        # Apply a set of standard options to each plot
        for p in self.plots.values():
            self._configure_plot_baseline(p)

        # Create the trigger framework
        self.triggerSource = None
        self.triggerFunction = lambda p, o, n: False
        self.triggerLine = pg.InfiniteLine(pos=self.voltageScale[1]/2, angle=0,
                            movable=True, pen=(245, 204, 126),
                            label="Trigger", labelOpts={"position": 0.1},
                            bounds=self.voltageScale)
        self.triggerLine.addMarker("o", position=0.05)

        # Specific plot configurations and adding graphics objects
        self.plots["xy"].setAspectLocked() # Keep it square
        self.plots["xy"].addItem(self.xyCurve) # Keep it square
        self.plots["zoom"].getAxis('left').setTicks([])
        self.plots["window"].addItem(self.triggerLine)

        # Set up the user-controllable zoom window
        self.create_zoom_window()

        # Set up initial plot ranges
        self.set_zoom_range([0, self.get_max_x()], self.voltageScale)
        self.set_xy_range(*self.voltageScale)

        self.set_window_layout()

    """Selecting a layout"""
    def set_window_layout(self):
        """Clear the graphics layout and display the zoom + window plots"""
        self.clear()

        self.addItem(self.plots["zoom"], 0, 0)
        self.addItem(self.plots["window"], 1, 0)

        # Relative Sizing
        self.ci.layout.setVerticalSpacing(-25)
        self.ci.layout.setRowStretchFactor(0, 1)
        self.ci.layout.setRowStretchFactor(1, 5)

    def set_xy_layout(self):
        """Clear the graphics layout and display the zoom + xy plots"""
        self.clear()

        self.addItem(self.plots["zoom"], 0, 0)
        self.addItem(self.plots["xy"], 1, 0)

        # Relative Sizing
        self.ci.layout.setVerticalSpacing(-25)
        self.ci.layout.setRowStretchFactor(0, 1)
        self.ci.layout.setRowStretchFactor(1, 5)

    """Cursors and channels"""
    def make_cursors(self, **kwargs):
        self.cursors = OscilloscopeTimeCursors(**kwargs)
        self.cursors.connect(self.plots["window"])

    def make_channel(self, channelName, pen):
        """Make a SignalChannel and add it to the plot

        The scope plot is in charge of updating channels
        with new data (fed from the outside) and making sure
        that curve boundaries are properly set.
        """
        # Make channel object
        self.channels[channelName] = SignalChannel(channelName, pen)

        # Attach each curve to the correct plot
        self.plots["zoom"].addItem(self.channels[channelName].get_full_curve())
        self.plots["window"].addItem(self.channels[channelName].get_zoomed_curve())

    def get_channel(self, channelName):
        """Return a the SignalChannel corresponding to the name"""
        return self.channels[channelName]

    def set_xy_source(self, channelNameX, channelNameY):
        """Set the channels for X and Y in the XY plot"""
        self.xySource = [self.channels[a] for a in [channelNameX, channelNameY]]

    def clear_channel_buffers(self):
        for channel in self.channels.values():
            channel.clear_buffers()

    def get_max_x(self):
        return self.dataRange * ScopePlot.TIMESCALE

    def new_data(self, data):
        """Filter new data into each signal channel as appropriate"""
        if len(self.channels) != len(data.values() or len(self.channels) == 0):
            logging.error("Do not have correct amount of channels set up")
            return

        if self.triggerSource == None:
            logging.warning("Trigger souce set to first channel as default")
            self.triggerSource = list(self.channels.keys())[0]

        # Put new data into each channel's hold buffer
        for key,channel in self.channels.items():
            # Extend the hold buffer with new data from feed
            channel.holdBuffer.extend(data[key])

        # Go through data for the channel, if there's a trigger condition, draw now
        # (if this channel is the trigger source)
        head = 1 # How many data points to draw (less than holdoff if we find trigger)
        triggerSourceBuffer = self.channels[self.triggerSource].holdBuffer
        holdoff = len(triggerSourceBuffer)

        # Scan through looking for trigger in the source buffer
        oldValue = triggerSourceBuffer[0]
        while (head < holdoff): # keep index boundary locked down
            value = triggerSourceBuffer[head]

            if (head > holdoff/2 and self._trigger_point(oldValue, value)):
                # Rising edge trigger, past plausible periodicity, draw now
                self.triggered.emit(self)
                break

            head += 1
            oldValue = value

        # At this point, head indicates where in the input buffers to actually draw data.
        # The rest will be put onto the hold buffer for next time around
        xAxis = np.array(range(self.dataRange)) * ScopePlot.TIMESCALE
        for key,channel in self.channels.items():
            # Get existing full data
            x,y = channel.get_data()

            # Shift off old data and add new data to the front
            frontShift = channel.holdBuffer[:head]
            shiftedData = self._buffer_clip(np.concatenate([y[head:], frontShift]), self.dataRange, self.dataRange)

            # Set data and keep the rest in holding for next time
            channel.holdBuffer = channel.holdBuffer[head:]
            self.update_bounded_channel(channel, xAxis, shiftedData)

        self.update_xy_curve()        

    def update_xy_curve(self):
        """Update drawing of the XY curve"""
        if (self.xySource != None and len(self.xySource) == 2):
            x, y = [channel.get_data_bounded()[1] for channel in self.xySource]
            self.xyCurve.setData(x, y)

    def update_bounded_channel(self, channel, xAxis=None, yAxis=None):
        """Update data as bounded, for performance"""
        x, y = channel.get_data()

        if xAxis is None:
            xAxis = x

        if yAxis is None:
            yAxis = y

        (start, end), (y1, y2) = self.get_window_range()

        channel.set_data_bounded([xAxis, yAxis], max(0, int(start / ScopePlot.TIMESCALE) - 10), int(end / ScopePlot.TIMESCALE) + 10)

    """Zoom window setup and control"""
    def create_zoom_window(self):
        self.zoomWindow = pg.LinearRegionItem(
            bounds=[0, self.get_max_x()]
        )
        self.zoomWindow.getBounds = lambda: [0, self.get_max_x()]

        self.reset_zoom_window()

        # These are to dark-out the rest of the plot and highlight only the window
        self.zoomCurtains = [pg.LinearRegionItem(
            movable=False
        ), pg.LinearRegionItem(
            movable=False
        )]
        self.update_main_window()

        self.zoomWindow.setBrush("#F5E8E8")
        self.zoomWindow.setHoverBrush("#F8EFEF")
        self.zoomWindow.lineMoved = self.update_main_window
        self.zoomWindow.wheelEvent = self.update_wheel_zoom

        self.plots["zoom"].addItem(self.zoomWindow)

        for curtain in self.zoomCurtains:
            curtain.setBrush("#6577BC")
            curtain.setZValue(1) # Put on top
            curtain.setOpacity(0.5)
            self.plots["zoom"].addItem(curtain)

        self.update_main_window()

    def update_main_window(self, event=None):

        # Manually hold region with minimum width
        start, end = self.zoomWindow.getRegion()
        if (end - start < self._min_zoom_width()):
            if end < self.get_max_x():
                start, end = start, start + self._min_zoom_width()
            else:
                start, end = end - self._min_zoom_width(), end
            self.zoomWindow.setRegion([start, end])

        # Update curtains
        self.zoomCurtains[0].setRegion([0, max(0, start)])
        self.zoomCurtains[1].setRegion([min(self.get_max_x(),end), self.get_max_x()])

        # Redraw the boundary curve when moving the window
        for channel in self.channels.values():
            self.update_bounded_channel(channel)

        self.update_xy_curve()        

        self.set_window_range(start, end)

    def update_wheel_zoom(self, event):
        start, end = self.zoomWindow.getRegion() # Zoom region, scale to percent of region
        z_min, z_max = self.zoomWindow.getBounds() # Zoom max and min, start scale
        zoomSpeed = self.get_data_range() / 300 # More data, need to zoom faster
        scaleAmount = max(2, ((zoomSpeed * (end - start))/(z_max - z_min) + 1))
        zoomDelta = scaleAmount * event.delta()/120
        self.bump_zoom_window(zoomDelta)

    def bump_zoom_window(self, amount=0):
        x, y = self.zoomWindow.getRegion()
        start, end = x - amount, y + amount
        
        if (end - start < self._min_zoom_width()):
            return # No bump if too small

        self.zoomWindow.setRegion([start, end])

    def reset_zoom_window(self):
        """Reset the position of the zoom window to a sane location"""
        if self.zoomWindow is None:
            return

        viewPad = self._min_zoom_width() * 3.2
        self.zoomWindow.setRegion([self.get_max_x()/2 - viewPad, self.get_max_x()/2 + viewPad])

    def _min_zoom_width(self):
        """Zoom width as a percent of the entire zoom region"""
        z_min, z_max = self.zoomWindow.getBounds()
        return ScopePlot.MIN_ZOOM_WIDTH/100 * (z_max - z_min)

    """Range control (data display boundaries)"""
    def set_data_range(self, datapoints=1024):
        """Change how much data we store in hold buffers each time"""
        datapoints = int(datapoints)
        if datapoints < ScopePlot.MIN_DATAPOINTS or datapoints > ScopePlot.MAX_DATAPOINTS:
            logging.warning("Attempted to set data range beyond acceptable limits (tried {}, range is {}-{})".format(datapoints, ScopePlot.MIN_DATAPOINTS, ScopePlot.MAX_DATAPOINTS))
            return

        self.dataRange = datapoints
        self.zoomWindow.setBounds([0, self.get_max_x()])
        self.set_zoom_range([0, self.get_max_x()], self.voltageScale)
        self.reset_zoom_window()
        self.update_main_window()

    def get_data_range(self):
        return self.dataRange

    def set_timescale(self, timescale):
        self.set_data_range(timescale / ScopePlot.TIMESCALE)

    def get_timescale(self):
        return self.get_data_range() * ScopePlot.TIMESCALE

    def set_voltage_scale(self, minimum, maximum):
        self.voltageScale = minimum, maximum
        self.set_xy_range(minimum, maximum)
        self.set_zoom_range([0, self.get_max_x()], self.voltageScale)
        self.update_main_window()

    def get_voltage_scale(self):
        return self.voltageScale

    def set_window_range(self, start, end):
        
        # Calculate boundaries
        x, y, w, h = start, self.voltageScale[0], end - start, (self.voltageScale[1] - self.voltageScale[0])

        # Set graph region
        self.plots["window"].setXRange(start, end, padding=0)
        self.plots["window"].setYRange(*self.voltageScale, padding=0.1)

    def get_window_range(self):
        """View range of the zoomed-in window. Also corresponds to window glyph in full plot"""
        return self.plots["window"].viewRange()

    def set_zoom_range(self, x_max_min, y_max_min):
        self.plots["zoom"].setRange(QtCore.QRectF(x_max_min[0], y_max_min[0], x_max_min[1] - x_max_min[0], y_max_min[1] - y_max_min[0]), padding=0)

    def get_zoom_range(self):
        return self.plots["zoom"].viewRange()

    def set_xy_range(self, v_min, v_max):
        """Set the XY plot to be equal-square axes with min and max values"""
        self.plots["xy"].setRange(xRange=(v_min,v_max), yRange=(v_min, v_max), padding=0.05)

    """Initialization"""
    def _configure_plot_baseline(self, plot):
        # Turn on the grid
        plot.showGrid(x=True, y=True)

        # Keep graph the right size
        plot.disableAutoRange()
        plot.hideButtons()

        # Turn off mouse mode
        plot.setMouseEnabled(x=False, y=False)
        plot.setMenuEnabled(False)

        # Show all axis items, but hide numbers on the others
        plot.showAxis('top')
        plot.showAxis('right')
        plot.getAxis('top').setTicks([])
        plot.getAxis('right').setTicks([])

    """Settings"""
    def set_channel_visibility(self, channelName, visible=True):
        self.channels[channelName].set_visible(visible)

    def get_trigger(self):
        return self.triggerLine.value()

    def set_trigger_function(self, func):
        self.triggerFunction = func

    def set_trigger_source(self, channelName):
        self.triggerSource = channelName

    def set_trigger_visible(self, state=True):
        self.triggerLine.setVisible(state)

    def set_cursors_visible(self, state=True):
        self.cursors.set_visible(state)

    def set_cursors_relative(self, state=True):
        self.cursors.set_relative_locked(state)

    def set_cursors_movable(self, state=True):
        self.cursors.set_movable(not state)

    def _trigger_point(self, old, new):
        """Compares previous value and current value to see if a trigger condition exists"""
        return self.triggerFunction(self.get_trigger(), old, new)

    def _buffer_clip(self, buff, clipsize, limit):
        diff = len(buff) - clipsize
        if diff < 0: # Need to add more zeros on to pad it out
            return np.concatenate([buff, np.zeros(-diff)])
        if len(buff) > limit : # Need to clip it down to size, cut off oldest data
            return buff[-limit:]

        # Otherwise fine
        return buff

class OscilloscopeTimeCursors():
    def __init__(self, pen='b--', movable=True, relativeLocked=True):
        """Represents a set of cursors on the screen

        Cursors are 4 lines, 2 horizontal, 2 vertical.

        Lines can have their visibility toggled.

        Cursors must be connected to a PlotItem after creation. This triggers
        initial positioning logic and a bounds update.

        If a plot's view range can change, connect this event to the 
        "update_relative_position" call, which will handle relative
        position locking (if enabled) and sane boundary updating.

        Parameters:
            pen: The pen literal used to style the lines
            movable: Whether or not the user can move the lines with their mouse
            relativeLocked: Whether or not the lines will be locked to the relative position
                            of the plot view rather than the data space.
        """
        # Objects
        self.plot = None
        self.linePairs = {
            # Lines where you select portions of the X axis (vertical)
            "x": (pg.InfiniteLine(label="X1: {value:.2f}", 
                    labelOpts={'color': '#324172', 'position': 0.55},
                    pos=50, pen=pen, angle=90),
                  pg.InfiniteLine(label="X2: {value:.2f}", 
                    labelOpts={'color': '#324172', 'position': 0.5},
                    pos=50, pen=pen, angle=90)), 

            # Lines where you select portions of the Y axis (horizontal)
            "y": (pg.InfiniteLine(label="Y1: {value:.2f}", 
                    labelOpts={'color': '#324172', 'position': 0.55},
                    pos=1, pen=pen, angle=0),
                  pg.InfiniteLine(label="Y2: {value:.2f}", 
                    labelOpts={'color': '#324172', 'position': 0.5},
                    pos=1, pen=pen, angle=0))   
        }

        self.deltas = {
            # Line that indicates the delta between x cursors
            "x": (QtGui.QGraphicsLineItem(), pg.TextItem(color='#58955F')),
            # Line that indicates the delta between y cursors
            "y": (QtGui.QGraphicsLineItem(), pg.TextItem(color='#58955F'))
        }
        for delta in self.deltas.values():
            delta[0].setPen(fn.mkPen({'color': '#269652', 'dash': [2, 2]}))
        self.update_deltas()

        # Settings
        self.movable = movable
        self.set_movable(movable)
        self.relativeLocked = relativeLocked
        self._relativePositions = None

    def connect(self, plot):
        """Connect these cursors to a particular plot"""
        self.plot = plot

        for o in self._graphics_objects():
            plot.addItem(o)
            o.setZValue(2) # Very on top

        for l in self._lines():
            l.sigPositionChangeFinished.connect(self._save_relative_positions)
            l.sigPositionChanged.connect(self.update_deltas)

        self.plot.getViewBox().sigRangeChanged.connect(self.update_bounds)
        self.update_bounds()
        self.reset_position()

    def disconnect(self):
        """Remove these cursors from the connected plot (if there is one)"""
        if self.plot is None:
            return       

        for o in self._graphics_objects():
            self.plot.removeItem(o)

        for l in self._lines():
            l.sigPositionChangeFinished.disconnect(self._save_relative_positions)
            l.sigPositionChanged.disconnect(self.update_deltas)

        self.plot.getViewBox().sigRangeChanged.disconnect(self.update_bounds)

    def reset_position(self):
        """Sets cursor lines to a sane initial position"""
        self.set_position()

    def set_position(self, proportions=(.1, .1, .1, .1)):
        """Sets cursor position to a percentage of the view size from top, right, bottom, left"""
        if self.plot is None:
            return

        xBounds, yBounds = self.plot.getViewBox().viewRange()

        # Top right bottom left, manual because looping is unnecessarily confusing
        self._set_relative_pos(self.linePairs["y"][0], proportions[0], yBounds)
        self._set_relative_pos(self.linePairs["x"][1], 1 - proportions[1], xBounds)
        self._set_relative_pos(self.linePairs["y"][1], 1 - proportions[2], yBounds)
        self._set_relative_pos(self.linePairs["x"][0], proportions[3], xBounds)

        self._relativePositions = proportions
        self.update_deltas()

    def set_movable(self, movable=True):
        """Toggle the movability of the cursor lines"""
        self.movable = movable
        for l in self._lines():
            l.setMovable(movable)

        self.update_bounds()

    def set_visible(self, visible=True):
        """Toggle the visibility of the cursors"""
        for o in self._graphics_objects():
            o.setVisible(visible)

    def set_relative_locked(self, relativeLocked=True):
        self.relativeLocked = relativeLocked

    def update_bounds(self, event=None):
        """Update the boundaries on each cursor to avoid losing them outside the view

        This is connected to any changes in the parent view object
        """
        if self.plot is None:
            return

        # Set movement limits if it's possible to loose track of cursors
        if self.movable:
            xBounds, yBounds = self.plot.getViewBox().viewRange()

            for l in self.linePairs["x"]:
                l.setBounds(xBounds)

            for l in self.linePairs["y"]:
                l.setBounds(yBounds)
        else:
            for l in self._lines():
                # Effectively remove bounds if the cursors are not movable (locked)
                l.setBounds((-math.inf, math.inf))

        # Handle relative position locking if turned on
        if self.relativeLocked and self._relativePositions != None:
            self.set_position(self._relativePositions)
        else:
            self.update_deltas()
            self._save_relative_positions()

    def update_deltas(self, event=None):
        """Moves the delta indicator lines and sets the delta text"""
        if self.plot is None:
            return

        xBounds, yBounds = self.plot.getViewBox().viewRange()
        xDiff, yDiff = [a[1] - a[0] for a in [xBounds, yBounds]]
        padScale = 0.02

        xValues, yValues = [[l.value() for l in self.linePairs[a]] for a in ["x", "y"]]

        # Standard spacing units for shifting things relative to axis values
        ySpaceUnit = padScale * yDiff
        xSpaceUnit = padScale * xDiff

        # Where deltas are clamped on each axis
        yClamp = yBounds[0] + ySpaceUnit
        xClamp = xBounds[0] + xSpaceUnit

        # Set positions and text labels for each object in turn
        # (Each text label is centered in the placement of the line)
        self.deltas["x"][0].setLine(xValues[0], yClamp, xValues[1], yClamp)
        self.deltas["x"][1].setPos(xValues[0] + (xValues[1] - xValues[0])/2, yClamp + 2 * ySpaceUnit)
        self.deltas["x"][1].setText("ΔX: {:.2f}".format(xValues[1] - xValues[0]))

        self.deltas["y"][0].setLine(xClamp, yValues[0], xClamp, yValues[1])
        self.deltas["y"][1].setPos(xClamp + xSpaceUnit/2, yValues[0] + (yValues[1] - yValues[0])/2)
        self.deltas["y"][1].setText("ΔY: {:.2f}".format(yValues[1] - yValues[0]))

    def _save_relative_positions(self):
        """Used to manage relative position locking"""
        xBounds, yBounds = self.plot.getViewBox().viewRange()

        self._relativePositions = (
            self._get_relative_pos(self.linePairs["y"][0], yBounds), # Top
            1 - self._get_relative_pos(self.linePairs["x"][1], xBounds), # Right
            1 - self._get_relative_pos(self.linePairs["y"][1], yBounds), # Bottom
            self._get_relative_pos(self.linePairs["x"][0], xBounds), # Left
        )

    def _get_relative_positions(self):
        return self._relativePositions

    def _set_relative_pos(self, line, frac, bounds):
        """Set the relative position based on boundaries"""
        diff = bounds[1] - bounds[0]
        line.setPos(frac * diff +  bounds[0])

    def _get_relative_pos(self, line, bounds):
        """Get the position as a fraction from the minimum of a boundary to max"""
        diff = bounds[1] - bounds[0]
        return (line.value() - bounds[0])/diff

    def _lines(self):
        """Get an enumerable of each cursor line"""
        (x1, x2), (y1, y2) = self.linePairs.values()
        return x1, x2, y1, y2

    def _graphics_objects(self):
        """Get an enumerable of each graphcis object"""
        (x1, x2), (y1, y2) = self.linePairs.values()
        (d1, t1), (d2, t2) = self.deltas.values()
        return x1, x2, y1, y2, d1, t1, d2, t2

class DragAndDropList(QtGui.QListWidget):
     itemMoved = QtCore.Signal(int, int, QtGui.QListWidgetItem) # Old index, new index, item

     def __init__(self, parent=None, **args):
         super(DragAndDropList, self).__init__(parent, **args)

         self.setAcceptDrops(True)
         self.setDragEnabled(True)
         self.setDragDropMode(QtGui.QAbstractItemView.InternalMove)
         self.drag_item = None
         self.drag_row = None

     def dropEvent(self, event):
         super(DragAndDropList, self).dropEvent(event)
         self.itemMoved.emit(self.drag_row, self.row(self.drag_item), self.drag_item)
         self.drag_item = None

     def startDrag(self, supportedActions):
         self.drag_item = self.currentItem()
         self.drag_row = self.row(self.drag_item)
         super(DragAndDropList, self).startDrag(supportedActions)

class ToggleButtons(QtGui.QWidget):
    toggled = QtCore.Signal(object)

    def __init__(self, left, right):
        super(ToggleButtons, self).__init__()

        self.state = True
        self.enabled = True
        self.labels = [left, right]

        container = QtGui.QGridLayout()
        container.setHorizontalSpacing(0)
        container.setContentsMargins(0, 0, 0, 0)

        self.buttons = [QtGui.QPushButton(label) for label in self.labels]

        for i,button in enumerate(self.buttons):
            button.clicked.connect(self._handle_button_click)
            button.setCursor(QtCore.Qt.PointingHandCursor)
            button.setStyleSheet("QPushButton { background: #E1E1E1; border: 1px solid #808080; margin: 0; padding: 3px; width: auto; }")
            container.addWidget(button, 0, i)
            container.setColumnStretch(i, 1)

        self.buttons[0].setStyleSheet("QPushButton { background: #E1E1E1; border: 1px solid #808080; margin: 0 -1px 0 0; padding: 3px; width: auto; }")

        self.setLayout(container)
        self.ui_update_enabled()

    def selected(self):
        """Returns 1 for left, 0 for right"""
        return 0 if self.state else 1

    def setButton(self, button):
        if (button == 0):
            newState = True
        else:
            newState = False


        if newState != self.state:
            self.state = newState   
            self.toggled.emit(self)

        self.ui_update_enabled()

    def setEnabled(self, enabled=True):
        self.enabled = enabled
        for button in self.buttons:
            button.setEnabled(enabled)

        if enabled:
            self.ui_update_enabled()

    def toggle(self):
        self._handle_button_click()

    def _handle_button_click(self):
        self.state = not self.state
        self.toggled.emit(self)
        self.ui_update_enabled()

    def ui_update_enabled(self):
        if self.enabled:
            stateCycle = cycle([self.state, not self.state])
            for button in self.buttons:
                button.setEnabled(next(stateCycle))

class OscilloscopeView(QtGui.QMainWindow):

    RETRY_DELAY = 0.5 # 500 milliseconds
    RESYNC_ATTEMPTS = 2 # How many times to search through
                        # channel blocks worth of data for a terminator
    CHANNELS = 2
    BLOCK_SIZE = 600 # Number of datapoints per payload frame
    POLL_RATE = 40 # times per second to poll for an updated frame
    PORTSELECT_MSG = "Select a serial port"
    NOPORT_MSG = "No ports available"

    def __init__(self):
        super(OscilloscopeView, self).__init__()
        self.device = None
        self.setGeometry(50, 50, 1200, 900)
        self.setWindowTitle("Decascope")

        # Update this to add more channels
        # The indexing order of these is what synchronises with the device channel markers
        # in each frame (which are sent starting at 0, maximum of 255)
        self.channelLabels = ["Channel 1", "Channel 2"]

        # Trigger direction comparators
        self.triggerDirections = {
            "Rising": (QtGui.QIcon(rpath("resources", "images", "trigger-rising-edge.png")), lambda point, old, new: old < point and new > point),
            "Falling": (QtGui.QIcon(rpath("resources", "images", "trigger-falling-edge.png")), lambda point, old, new: old > point and new < point),
            "Both": (QtGui.QIcon(rpath("resources", "images", "trigger-both.png")), lambda point, old, new: (old < point and new > point) or (old > point and new < point)),
        }

        # Measurement functions
        self.measurementFunctions = {
            "Peak to Peak"  : (QtGui.QIcon(rpath("resources", "images", "peak-to-peak.png")), SigMeas.peak_to_peak, "Vpp({channels}) = {results}"),
            "Frequency"     : (QtGui.QIcon(rpath("resources", "images", "frequency.png"))   , SigMeas.frequency,    "freq({channels}) = {results}"),
            "Period"        : (QtGui.QIcon(rpath("resources", "images", "period.png"))      , SigMeas.period,       "T({channels}) = {results}"),
            "Max"           : (QtGui.QIcon(rpath("resources", "images", "max.png"))         , SigMeas.max,          "max({channels}) = {results}"),
            "Average"       : (QtGui.QIcon(rpath("resources", "images", "average.png"))     , SigMeas.average,      "avg({channels}) = {results}"),
            "Min"           : (QtGui.QIcon(rpath("resources", "images", "min.png"))         , SigMeas.min,          "min({channels}) = {results}"),
            # "Phase (1 > 2)" : (QtGui.QIcon(rpath("resources", "images", "phase-1-2.png"))   , SigMeas.phase_1_2,    "phase({channels}) = {results}"),
            # "Phase (2 > 1)" : (QtGui.QIcon(rpath("resources", "images", "phase-2-1.png"))   , SigMeas.phase_2_1,    "phase({channels}) = {results}"),
        }

        # For exporting screenshots and channel data
        self.outputDirectory = None

        # Populate with main layout
        self.create_layout()
        self.ui_update_ports()

        # Possible auto-connect to only serial device
        ports = self.get_available_ports()
        if (len(ports) == 1):
            self.serialPort = ports[0]

        # Make the user cursors
        self.scopePlot.make_cursors(movable=True, pen={'color': '#1970D2', 'width': 1.5, 'dash': [6, 3]})
        self.scopePlot.set_cursors_visible(False)

        # Make the channels for the scope plot
        colors = [(27, 112, 217), (106, 224, 20)]
        for i,label in enumerate(self.channelLabels):
            self.scopePlot.make_channel(label, colors[i])

        # Set first two channels as source for XY
        self.scopePlot.set_xy_source(*self.channelLabels[:2])

        # For graphical update events
        self.updateTimer = QtCore.QTimer()
        self.updateTimer.timeout.connect(self.update)
        self.updateTimer.start(1000/OscilloscopeView.POLL_RATE)

        # For measurement recalculation events
        self.measurementUpdateTimer = QtCore.QTimer()
        self.measurementUpdateTimer.timeout.connect(self.ui_update_measurements)
        self.measurementUpdateTimer.start(500)

        # For intermittent update events
        self.slowUiUpdateTimer = QtCore.QTimer()
        self.slowUiUpdateTimer.timeout.connect(self.slow_update)
        self.slowUiUpdateTimer.start(2000)

        self.ui_update_visibility()
        self.show()
        self.serialSelectBox.setFocus()

    def create_layout(self):

        # Master widget
        self.master = QtGui.QWidget()
        self.setCentralWidget(self.master)

        # <Main Grid>
        grid = QtGui.QGridLayout()
        self.master.setLayout(grid)

        #   <Top Row>
        topRowLayout = QtGui.QGridLayout()
        topRowLayout.setContentsMargins(36, 10, 43, 0)
        topRowWidget = QtGui.QWidget()

        #       <Main Label>
        mainLabelLayout = QtGui.QVBoxLayout()
        mainLabelWidget = QtGui.QWidget()

        titleFont = QtGui.QFont("Segoe UI Light", 22, QtGui.QFont.Normal)
        subtitleFont = QtGui.QFont("Segoe UI Light", 10, QtGui.QFont.Normal)
        subtleFont = QtGui.QFont("Segoe UI", 7, QtGui.QFont.Normal)

        #           <Title>
        title = QtGui.QLabel("Decascope Monitor")
        title.setStyleSheet("QLabel {color:#2E2E41;}")
        title.setFont(titleFont)
        title.setContentsMargins(-3,0,0,0)
        title.adjustSize()
        #           </Title>
        #           <Subtitle>
        subtitle = QtGui.QLabel("Decatronics, by Jason - v{}.{}".format(VERSION_MAJ, VERSION_MIN))
        subtitle.setStyleSheet("QLabel {color:#9797BE;}")
        subtitle.setFont(subtitleFont)
        subtitle.adjustSize()
        #           </Subtitle>

        mainLabelLayout.addWidget(title)
        mainLabelLayout.addWidget(subtitle)
        mainLabelWidget.setLayout(mainLabelLayout)
        #       </Main Label>

        #       <Vertical Scale Settings>
        verticalScaleLayout = QtGui.QGridLayout()
        verticalScaleLayout.setContentsMargins(5, 0, 5, 7)
        verticalScaleLayout.setHorizontalSpacing(2)
        verticalScaleLayout.setVerticalSpacing(2)
        self.verticalScaleGroup = QtGui.QGroupBox("Voltage Scale")

        #           <Vertical Scale Labels>
        verticalScaleMinLabel = QtGui.QLabel("Min")
        verticalScaleMinLabel.setFont(subtleFont)
        verticalScaleMinLabel.adjustSize()

        verticalScaleMaxLabel = QtGui.QLabel("Max")
        verticalScaleMaxLabel.setFont(subtleFont)
        verticalScaleMaxLabel.adjustSize()
        #           </Vertical Scale Labels>

        #           <Vertical Scale Spin Boxes>
        self.verticalScaleMinSpin = QtGui.QSpinBox()
        self.verticalScaleMinSpin.setToolTip("Minimum voltage")
        self.verticalScaleMinSpin.setMinimum(-200)
        self.verticalScaleMinSpin.setMaximum(200)
        self.verticalScaleMinSpin.setMaximumWidth(50)
        self.verticalScaleMinSpin.valueChanged.connect(self.ui_update_vertical_scale)

        self.verticalScaleMaxSpin = QtGui.QSpinBox()
        self.verticalScaleMaxSpin.setToolTip("Maximum voltage")
        self.verticalScaleMaxSpin.setMinimum(-200)
        self.verticalScaleMaxSpin.setMaximum(200)
        self.verticalScaleMaxSpin.setMaximumWidth(50)
        self.verticalScaleMaxSpin.valueChanged.connect(self.ui_update_vertical_scale)
        #           </Vertical Scale Spin Boxes>

        verticalScaleLayout.addWidget(verticalScaleMinLabel, 0, 0)
        verticalScaleLayout.addWidget(verticalScaleMaxLabel, 0, 1)
        verticalScaleLayout.addWidget(self.verticalScaleMinSpin, 1, 0)
        verticalScaleLayout.addWidget(self.verticalScaleMaxSpin, 1, 1)
        self.verticalScaleGroup.setLayout(verticalScaleLayout)
        #       </Vertical Scale Settings>

        #       <Serial Connection Settings>
        serialSettingsLayout = QtGui.QGridLayout()
        serialSettingsLayout.setContentsMargins(6, 16, 6, 6)
        self.serialSettingsBox = QtGui.QGroupBox("Serial Settings")

        #           <Connect Button>
        self.serialConnectBtn = QtGui.QPushButton("Connect")
        self.serialConnectBtn.setCursor(QtCore.Qt.PointingHandCursor)
        self.serialConnectBtn.clicked.connect(self.ui_open_serial)
        #           </Connect Button>
        #           <Port Select Box
        self.serialSelectBox = QtGui.QComboBox()
        self.serialSelectBox.setStyleSheet("QComboBox { min-width: 100px; padding: 1px 12px 2px 5px; }") # Fix the default styling? Windows 10 only
        self.serialSelectBox.setCursor(QtCore.Qt.PointingHandCursor)
        self.serialSelectBox.enterEvent = self.ui_update_ports
        #           </Port Select Box>
        
        serialSettingsLayout.addWidget(self.serialConnectBtn, 0, 0)
        serialSettingsLayout.addWidget(self.serialSelectBox, 0, 1)
        serialSettingsLayout.setColumnStretch(1, 1)
        self.serialSettingsBox.setLayout(serialSettingsLayout)
        #       </Serial Connection Settings>

        topRowLayout.addWidget(mainLabelWidget, 0, 0, 2, 1)
        topRowLayout.addWidget(self.verticalScaleGroup, 1, 1)
        topRowLayout.addWidget(self.serialSettingsBox, 1, 2)
        topRowLayout.setColumnStretch(0, 8)
        topRowLayout.setColumnStretch(1, 0)
        topRowLayout.setColumnStretch(2, 2)
        topRowLayout.setRowStretch(0, 1)
        topRowWidget.setLayout(topRowLayout)
        #   </Top Row>

        # <Main plot>
        self.scopePlot = ScopePlot()

        # Set default values on scales
        v_min, v_max = self.scopePlot.get_voltage_scale()
        self.verticalScaleMinSpin.setValue(v_min)
        self.verticalScaleMaxSpin.setValue(v_max)
        # </Main plot>

        # <Bottom Row>
        bottomRowLayout = QtGui.QGridLayout()
        bottomRowWidget = QtGui.QWidget()

        #   <Visibility Settings>
        visibilityLayout = QtGui.QVBoxLayout()
        visibilitySettingsGroup = QtGui.QGroupBox("Signal Visibility")
        self.visibilityState = {}

        #       <Trigger Visibility>
        self.visibilityState["Trigger"] = QtGui.QCheckBox("Trigger")
        self.visibilityState["Trigger"].setCursor(QtCore.Qt.PointingHandCursor)
        #       </Trigger Visibility>

        #       <Channel Visibilities>
        for c in self.channelLabels:
            self.visibilityState[c] = QtGui.QCheckBox(c)
            self.visibilityState[c].setChecked(True)
            self.visibilityState[c].setCursor(QtCore.Qt.PointingHandCursor)
        #       <Channel Visibilities>

        for x in self.visibilityState.keys():
            self.visibilityState[x].toggled.connect(self.ui_update_visibility)
            visibilityLayout.addWidget(self.visibilityState[x])

        visibilitySettingsGroup.setLayout(visibilityLayout)
        #   </Visibility Settings>

        #   <Trigger Settings>
        triggerSettingsLayout = QtGui.QGridLayout()
        triggerSettingsGroup = QtGui.QGroupBox("Trigger Settings")

        #       <Trigger Source Label>
        triggerSourceLabel = QtGui.QLabel("Source:")
        #       </Trigger Source Label>

        #       <Trigger Source Select>
        self.triggerSourceSelect = QtGui.QComboBox()
        self.triggerSourceSelect.setCursor(QtCore.Qt.PointingHandCursor)
        self.triggerSourceSelect.currentIndexChanged.connect(self.ui_update_trigger_source)
        for c in self.channelLabels: # Select any channel as the source
            self.triggerSourceSelect.addItem(c)
        #       </Trigger Source Select>

        #       <Trigger Direction Label>
        triggerDirectionLabel = QtGui.QLabel("Direction:")
        #       </Trigger Direction Label>
        
        #       <Trigger Direction Select>
        self.triggerDirectionSelect = QtGui.QComboBox()
        self.triggerDirectionSelect.setStyleSheet("QComboBox { spacing: 6px; padding: -1.5px -0.5px; min-width: 80px; } QComboBox QAbstractItemView { padding: 0 -2px;} QComboBox QAbstractItemView::item { padding: -1px;}")
        self.triggerDirectionSelect.setCursor(QtCore.Qt.PointingHandCursor)
        self.triggerDirectionSelect.setView(QtGui.QListView())
        self.triggerDirectionSelect.currentIndexChanged.connect(self.ui_update_trigger_direction)
        for direction in self.triggerDirections.keys():
            self.triggerDirectionSelect.addItem(self.triggerDirections[direction][0], direction)
        self.triggerDirectionSelect.setIconSize(QtCore.QSize(35 , 35))
        #       </Trigger Direction Select>

        triggerSettingsLayout.addWidget(triggerSourceLabel, 0, 0)
        triggerSettingsLayout.addWidget(self.triggerSourceSelect, 0, 1)
        triggerSettingsLayout.addWidget(triggerDirectionLabel, 1, 0)
        triggerSettingsLayout.addWidget(self.triggerDirectionSelect, 1, 1)
        triggerSettingsGroup.setLayout(triggerSettingsLayout)
        #   </Trigger Settings>

        #   <Measurement Settings>
        measurementLayout = QtGui.QGridLayout()
        measurementGroup = QtGui.QGroupBox("Measurements")

        #       <Measurement Source Label>
        measurementSourceLabel = QtGui.QLabel("Source:")
        #       </Measurement Source Label>

        #       <Measurement Source>
        self.measurementSourceSelect = QtGui.QComboBox()
        self.measurementSourceSelect.setCursor(QtCore.Qt.PointingHandCursor)
        for c in self.channelLabels: # Select any channel as the source
            self.measurementSourceSelect.addItem(c)
        #       </Measurement Source>

        #       <Measurement Type Label>
        measurementLabel = QtGui.QLabel("Type:")
        #       </Measurement Type Label>

        #       <Measurement Selection>
        self.measurementSelect = QtGui.QComboBox()
        self.measurementSelect.setStyleSheet("QComboBox { spacing: 6px; padding: -1.5px -0.5px; min-width: 80px; } QComboBox QAbstractItemView { padding: 0 -2px;} QComboBox QAbstractItemView::item { padding: -1px;}")
        self.measurementSelect.setCursor(QtCore.Qt.PointingHandCursor)
        self.measurementSelect.setView(QtGui.QListView())
        for name in self.measurementFunctions.keys():
            self.measurementSelect.addItem(self.measurementFunctions[name][0], name)
        self.measurementSelect.setIconSize(QtCore.QSize(35 , 35))
        #       </Measurement Selection>

        #       <Add Measurement Button>
        measurementAddButton = QtGui.QPushButton("Add \u2192")
        measurementAddButton.setCursor(QtCore.Qt.PointingHandCursor)
        measurementAddButton.clicked.connect(self.ui_add_measurement)
        #       </Add Measurement Button>

        #       <Measurement Display List>
        self.measurementList = DragAndDropList()
        self.measurementList.setStyleSheet("QListWidget { max-width: 140px; min-height: 110px; }")
        #       </Measurement Display List>

        measurementLayout.addWidget(measurementSourceLabel, 0, 0)
        measurementLayout.addWidget(self.measurementSourceSelect, 0, 1)
        measurementLayout.addWidget(measurementLabel, 1, 0)
        measurementLayout.addWidget(self.measurementSelect, 1, 1)
        measurementLayout.addWidget(measurementAddButton, 2, 1)
        measurementLayout.addWidget(self.measurementList, 0, 3, 3, 1)
        measurementGroup.setLayout(measurementLayout)
        #   </Measurement Settings>

        #   <Cursors Settings>
        cursorSettingsLayout = QtGui.QVBoxLayout()
        cursorSettingsBox = QtGui.QGroupBox("Cursors")

        #       <Cursor Visibility>
        self.cursorVisibilityCheckbox = QtGui.QCheckBox("Visible")
        self.cursorVisibilityCheckbox.setCursor(QtCore.Qt.PointingHandCursor)
        self.cursorVisibilityCheckbox.toggled.connect(self.ui_update_visibility)
        #       </Cursor Visibility>

        #       <Cursor Relative>
        self.cursorRelativeCheckbox = QtGui.QCheckBox("Relative")
        self.cursorRelativeCheckbox.setCursor(QtCore.Qt.PointingHandCursor)
        self.cursorRelativeCheckbox.setToolTip("Cursors move with view")
        self.cursorRelativeCheckbox.setChecked(True)
        self.cursorRelativeCheckbox.toggled.connect(
            lambda e: self.scopePlot.set_cursors_relative(self.cursorRelativeCheckbox.isChecked())
        )
        #       </Cursor Relative>

        #       <Cursor Position Lock>
        self.cursorLockCheckbox = QtGui.QCheckBox("Locked")
        self.cursorLockCheckbox.setCursor(QtCore.Qt.PointingHandCursor)
        self.cursorLockCheckbox.setToolTip("Lock cursor position")
        self.cursorLockCheckbox.toggled.connect(
            lambda e: self.scopePlot.set_cursors_movable(self.cursorLockCheckbox.isChecked())
        )
        #       </Cursor Position Lock>

        cursorSettingsLayout.addWidget(self.cursorVisibilityCheckbox)
        cursorSettingsLayout.addWidget(self.cursorRelativeCheckbox)
        cursorSettingsLayout.addWidget(self.cursorLockCheckbox)
        cursorSettingsBox.setLayout(cursorSettingsLayout)
        #   </Cursors Settings>

        #   <Plot Mode Settings>
        plotModeSettingsLayout = QtGui.QGridLayout()
        plotModeSettingsGroup = QtGui.QGroupBox("Mode")

        #       <Start-Stop>
        self.startStopModeButtons = ToggleButtons("Start", "Stop")
        self.startStopModeButtons.toggle()
        self.startStopModeButtons.toggled.connect(self.ui_update_run_mode)
        #       </Start-Stop>

        #       <Plotting mode>
        self.plotModeButtons = ToggleButtons("Time", "XY")
        self.plotModeButtons.toggle()
        self.plotModeButtons.toggled.connect(self.ui_update_plot_mode)
        #       </Plotting mode>

        #       <Capture Button>
        self.captureModeButton = QtGui.QPushButton("Capture")
        self.captureModeButton.setCursor(QtCore.Qt.PointingHandCursor)
        self.captureModeButton.setToolTip("Wait for a trigger, then do full sweep and stop.")
        self.captureModeButton.clicked.connect(self.ui_start_capture)
        #       </Capture Button>

        #       <Horizontal Scale Label>
        horizontalScaleLabel = QtGui.QLabel("Timescale (ms)")
        horizontalScaleLabel.setToolTip("Minimum 50ms, maximum 5000ms")
        #       </Horizontal Scale Label>

        #       <Horizontal Scale>
        self.horizontalScaleSpin = QtGui.QSpinBox()
        self.horizontalScaleSpin.setToolTip("Minimum 50ms, maximum 5000ms")
        self.horizontalScaleSpin.setMinimum(ScopePlot.MIN_TIMESCALE)
        self.horizontalScaleSpin.setMaximum(ScopePlot.MAX_TIMESCALE)
        self.horizontalScaleSpin.setValue(self.scopePlot.get_timescale())
        self.horizontalScaleSpin.valueChanged.connect(self.ui_update_timescale)
        #       </Horizontal Scale>

        plotModeSettingsLayout.addWidget(self.startStopModeButtons)
        plotModeSettingsLayout.addWidget(self.plotModeButtons)
        plotModeSettingsLayout.addWidget(self.captureModeButton)
        plotModeSettingsLayout.addWidget(horizontalScaleLabel)
        plotModeSettingsLayout.addWidget(self.horizontalScaleSpin)
        plotModeSettingsGroup.setLayout(plotModeSettingsLayout)
        #   </Plot Mode Settings>

        #   <Export Settings>
        exportSettingsLayout = QtGui.QGridLayout()
        exportSettingsLayout.setVerticalSpacing(0)
        exportSettingsGroup = QtGui.QGroupBox("Export")

        #       <Ouput Directory Button>
        outputDirectoryButton = QtGui.QPushButton("Output Location")
        outputDirectoryButton.setMinimumWidth(100)
        outputDirectoryButton.setCursor(QtCore.Qt.PointingHandCursor)
        outputDirectoryButton.setToolTip("Set the output directory for exports")
        outputDirectoryButton.clicked.connect(self.ui_set_export_directory)
        #       </Ouput Directory Button>

        #       <Export Scope Label>
        exportScopeLabel = QtGui.QLabel("Image")
        exportScopeLabel.setFont(subtleFont)
        exportScopeLabel.setContentsMargins(0, 10, 0, 0)
        exportScopeLabel.adjustSize()
        #       </Export Scope Label>

        #       <Export Scope Button>
        self.exportScopeButton = QtGui.QPushButton("Export Scope")
        self.exportScopeButton.setEnabled(False)
        self.exportScopeButton.setCursor(QtCore.Qt.PointingHandCursor)
        self.exportScopeButton.setToolTip("Export scope as a PNG image")
        self.exportScopeButton.clicked.connect(self.ui_export_scope)
        #       </Export Scope Button>

        #       <Export Data Label>
        exportDataLabel = QtGui.QLabel("Data file")
        exportDataLabel.setContentsMargins(0, 10, 0, 0)
        exportDataLabel.setFont(subtleFont)
        exportDataLabel.adjustSize()
        #       </Export Data Label>

        #       <Export Data Button>
        self.exportDataButton = QtGui.QPushButton("Export Data")
        self.exportDataButton.setEnabled(False)
        self.exportDataButton.setCursor(QtCore.Qt.PointingHandCursor)
        self.exportDataButton.setToolTip("Export channel data as a CSV")
        self.exportDataButton.clicked.connect(self.ui_export_data)
        #       </Export Data Button>

        exportSettingsLayout.addWidget(outputDirectoryButton, 0, 0)
        exportSettingsLayout.addWidget(exportScopeLabel, 1, 0)
        exportSettingsLayout.addWidget(self.exportScopeButton, 2, 0)
        exportSettingsLayout.addWidget(exportDataLabel, 3, 0)
        exportSettingsLayout.addWidget(self.exportDataButton, 4, 0)
        exportSettingsGroup.setLayout(exportSettingsLayout)
        #   </Export Settings>

        # Axes units label
        self.unitsLabel = QtGui.QLabel("(Axis units: V/ms)")
        self.unitsLabel.setStyleSheet("QLabel { color: #76819B; margin: 0 0 0 5px; }")
        bottomRowLayout.addWidget(self.unitsLabel, 0, 0)

        groupLayouts = [visibilitySettingsGroup, triggerSettingsGroup, measurementGroup, 
                        cursorSettingsBox, plotModeSettingsGroup, exportSettingsGroup]
        for i,group in enumerate(groupLayouts):
            bottomRowLayout.addWidget(group, 1, i)
        bottomRowLayout.addWidget(QtGui.QWidget(), 1, len(groupLayouts)) # Padding out
        bottomRowLayout.setColumnStretch(len(groupLayouts), 1)

        bottomRowLayout.setContentsMargins(36, 0, 43, 20)
        bottomRowWidget.setLayout(bottomRowLayout)
        # </Bottom Row>

        # Add all the widgets and set stretching weights
        grid.addWidget(topRowWidget, 0, 0)
        grid.addWidget(self.scopePlot, 1, 0)
        grid.addWidget(bottomRowWidget, 2, 0)
        grid.setRowStretch(1, 1)
        # </Main Grid>

    def ui_update_visibility(self, event=None):
        self.scopePlot.set_trigger_visible(self.visibilityState["Trigger"].isChecked())
        self.scopePlot.set_cursors_visible(self.cursorVisibilityCheckbox.isChecked())

        for box in [self.visibilityState[x] for x in self.channelLabels]:
            self.scopePlot.set_channel_visibility(box.text(), box.isChecked())

    def ui_update_trigger_direction(self, event=None):
        direction = self.triggerDirectionSelect.currentText()
        self.scopePlot.set_trigger_function(self.triggerDirections[direction][1])

    def ui_update_trigger_source(self, event=None):
        self.scopePlot.set_trigger_source(self.triggerSourceSelect.currentText())

    def ui_add_measurement(self, event=None):
        measurement = self.measurementFunctions[self.measurementSelect.currentText()]
        signalMeasurement = SigMeas(measurement[1], measurement[2])
        signalMeasurement.connect(self.scopePlot.get_channel(self.measurementSourceSelect.currentText()))

        # Check that the would-be signal measurement isn't already present
        for i in range(self.measurementList.count()):
            if self.measurementList.item(i).signalMeasurement == signalMeasurement:
                return

        # Construct widget to display item
        # NOTE: Attach to list item for later access
        measListItem = QtGui.QListWidgetItem()
        measListItem.signalMeasurement = signalMeasurement
        measListItem.label = QtGui.QLabel(str(signalMeasurement))
        measWidget = QtGui.QWidget()
        measLayout = QtGui.QHBoxLayout()
        measBtn  = QtGui.QPushButton("-")
        measBtn.clicked.connect(lambda e: self.ui_remove_measurement(signalMeasurement))
        measBtn.setToolTip("Delete measurement")
        measBtn.setCursor(QtCore.Qt.PointingHandCursor)
        measLayout.addWidget(measListItem.label)
        measLayout.addWidget(measBtn)
        measLayout.setContentsMargins(10,0,0,0)
        measWidget.setLayout(measLayout)
        measWidget.setStyleSheet("QPushButton { min-width: 13px; max-width: 13px; padding: 3px; }")
        measListItem.setSizeHint(measWidget.sizeHint())

        # Add item to the list
        self.measurementList.addItem(measListItem)
        self.measurementList.setItemWidget(measListItem, measWidget)

    def ui_update_measurements(self, event=None):
        """Recalculate each measurement and update the text object"""
        for i in range(self.measurementList.count()):
            sigMeas = self.measurementList.item(i).signalMeasurement
            sigMeas.update()
            self.measurementList.item(i).label.setText(str(sigMeas))

    def ui_remove_measurement(self, measurement):
        """Remove a unique measurement from the list of measurements"""
        for i in range(self.measurementList.count()):
            if self.measurementList.item(i).signalMeasurement == measurement:
                self.measurementList.takeItem(i)
                return

    def ui_update_run_mode(self, event=None):
        if (self.startStopModeButtons.selected()):
            self.clear_buffers()
            self.updateTimer.start(1000/OscilloscopeView.POLL_RATE)
        else:
            self.updateTimer.stop()
            self.clear_buffers()

    def ui_update_plot_mode(self, event=None):
        if (self.plotModeButtons.selected()):
            self.scopePlot.set_window_layout()
        else:
            self.scopePlot.set_xy_layout()

    def ui_start_capture(self, event=None):
        # Do not allow interaction with stop-start buttons
        self.horizontalScaleSpin.setEnabled(False)
        self.startStopModeButtons.setEnabled(False)
        
        # Wait for trigger
        self.startStopModeButtons.setButton(1) # Will start trace

        # Connect to triggered event in the plot
        # Also allow the user to stop the capture
        # Don't allow manual start-stop toggling
        self.captureModeButton.setText("Stop Capture")
        reconnect(self.captureModeButton.clicked, self.ui_end_capture, self.ui_start_capture)
        reconnect(self.scopePlot.triggered, self.ui_sweep_capture, None)

    def ui_sweep_capture(self, event=None):
        # Don't allow interaction
        self.captureModeButton.setEnabled(False)
        self.captureModeButton.setText("Capturing...")

        # Stop the capture after the interval is reached (need time for ISR to run)
        self.uiCaptureTimer = QtCore.QTimer()
        self.uiCaptureTimer.timeout.connect(self.ui_end_capture)
        self.uiCaptureTimer.start(int(self.scopePlot.get_timescale() / 1.05 - 20))

        reconnect(self.scopePlot.triggered, None, self.ui_sweep_capture)

    def ui_end_capture(self, event=None):
        reconnect(self.captureModeButton.clicked, self.ui_start_capture, self.ui_end_capture)
        reconnect(self.scopePlot.triggered, None, self.ui_sweep_capture)

        try:
            self.uiCaptureTimer.stop()
            self.uiCaptureTimer = None
            self.startStopModeButtons.setButton(0) # Will stop trace
        except:
            pass # Just means premature stop

        # Re-enable interaction
        self.horizontalScaleSpin.setEnabled(True)
        self.captureModeButton.setText("Capture")
        self.captureModeButton.setEnabled(True)
        self.startStopModeButtons.setEnabled(True)

    def ui_update_timescale(self, event=None):
        self.scopePlot.set_timescale(self.horizontalScaleSpin.value())

    def ui_update_vertical_scale(self, event=None):
        """Pull numbers from the min and max spins and set vertical scale"""

        v_min, v_max = self.verticalScaleMinSpin.value(), self.verticalScaleMaxSpin.value()
        if v_min >= v_max:
            self.verticalScaleGroup.setTitle("Voltage Scale (min < max)")
        else:
            self.verticalScaleGroup.setTitle("Voltage Scale")
            self.scopePlot.set_voltage_scale(v_min, v_max)

    def ui_set_export_directory(self):
        response = QtGui.QFileDialog.getExistingDirectory()
        if len(response) > 0:
            self.outputDirectory = response

        self.ui_update_enabled()

    def ui_export_scope(self):
        if not self.valid_output_directory():
            logging.warning("Attempted scope export to bad path")
            return # Failed because path is invalid

        # Make image
        sceneRect = self.scopePlot.viewRect()
        w, h = sceneRect.width(), sceneRect.height()
        img = QtGui.QImage(w, h, QtGui.QImage.Format_ARGB6666_Premultiplied)

        # Paint scene into image
        painter = QtGui.QPainter(img)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        self.scopePlot.render(painter)
        del painter

        # Save image
        outputPath = self.get_next_export_path("scope_screenshot", ".png")
        img.save(outputPath)

    def ui_export_data(self):
        if not self.valid_output_directory():
            logging.warning("Attempted data export to bad path")
            return # Failed because path is invalid

        outputPath = self.get_next_export_path("scope_data", ".csv")
        exporter = pg.exporters.CSVExporter(self.scopePlot.mainPlot)
        exporter.export(outputPath)

    def get_next_export_path(self, prefix, extension=".txt"):
        if not self.valid_output_directory():
            logging.info("Couldn't attempt next path resolution")
            return

        limit = 1000 # Just in case
        i = 0

        while (i < limit):
            attemptPath = path.join(self.outputDirectory, "{prefix}_{num}{ext}"
                .format(prefix=prefix, num=i, ext=extension))

            if (not path.exists(attemptPath)):
                return attemptPath

            i += 1

        # Only when failed to find a new path
        return prefix + extension

    def valid_output_directory(self):
        return not self.outputDirectory == None and path.exists(self.outputDirectory)

    def ui_update_enabled(self):
        """Update which elements should be enabled or disabled currently"""
        validExportPath = self.valid_output_directory()
        self.exportScopeButton.setEnabled(validExportPath)
        self.exportDataButton.setEnabled(validExportPath)
    
    def ui_update_ports(self, event=None):
        ports = self.get_available_ports()

        # Disable button if no ports
        self.serialConnectBtn.setDisabled(len(ports) == 0 or self._is_connected())

        if len(ports) == 0:
            ports.append("No ports")

        self.serialSelectBox.clear()
        for p in ports:
            self.serialSelectBox.addItem(p)

        # Check connection indicator
        if self._is_connected():
            self.serialSettingsBox.setTitle("Serial Settings (Connected)")
        else:
            self.serialSettingsBox.setTitle("Serial Settings (Disconnected)")

    def get_available_ports(self):
        portList = list_ports.comports()
        return [x.device for x in portList]

    def ui_open_serial(self, event):
        self.serialPort = self.serialSelectBox.currentText()
        try:
            self.open_serial()
        except IOError:
            logging.warning("Error opening serial port")

    def open_serial(self):
        self.close_serial()
        try:
            self.device = serial.Serial(self.serialPort, 230400)
            self.clear_buffers()
        except:
            raise IOError("Unable to open serial port")
            
        # Wait for ready message from device
        startupMessage = str(self.device.readline())
        if ('Decascope' not in str(startupMessage)):
            self.close_serial()
            raise IOError("Scope unrecognised (try again)")

        # Disable the connect button immediately
        self.serialConnectBtn.setDisabled(True)

    def close_serial(self):
        if (self.device != None):
            self.device.close()

    def slow_update(self):

        # Serial connection indicators
        self.ui_update_ports()

        # Just in case it somehow glitches out of sync
        self.ui_update_visibility()

        # To keep the buttons set in case they are missed
        self.ui_update_enabled()

    def update(self):
        if (not self._is_connected()):
            try:
                self.open_serial()
                self.ui_update_ports()
            except IOError:
                # Failed to reconnect to device
                pass
            return # Nothing to do

        # Get next data blocks
        # Number of datapoints to wait until forcing a waveform update (no trigger)
        holdoff = OscilloscopeView.BLOCK_SIZE

        # Get actual data from the device
        data = None
        try:
            data = self.request_device_data(holdoff)
            if data == None:
                raise IOError("Data request returned empty")
        except IOError as e:
            # Device malfunction - possibly disconnected or resync failures
            self.close_serial()
            return

        # Feed data into the plot
        self.scopePlot.new_data(data)

    def request_device_data(self, minPoints=None):
        """Read data from device until we have enough in each buffer (split channel streams)"""
        if (minPoints == None):
            minPoints = OscilloscopeView.BLOCK_SIZE

        # Enforce that we read all channels in sequence
        # Need to use a numeric sequence to compare with channel markers from device
        channelSequence = cycle(range(len(self.channelLabels)))
        currentChannel = next(channelSequence)

        # Initialize empty buffers for each channel to feed into the scope
        channelBuffers = {}
        for label in self.channelLabels:
            channelBuffers[label] = []

        # Keep reading until we have enough data in each channel buffer
        while (min([len(buff) for buff in channelBuffers.values()]) < minPoints):
            # Assume first datapoint is a 1-based channel number (if it isn't, resync and try again)
            x = self._next_channel_block_start() - 1

            # Make sure we're at the start of a block and we're in sequence
            if (x != currentChannel or x == -1):
                self.resync()
                continue

            # Get BLOCK_SIZE datapoints and scale it to size
            datapoints = self.device.read(OscilloscopeView.BLOCK_SIZE)
            scaledData = np.array(list(datapoints)) * (self.scopePlot.get_voltage_scale()[1] - self.scopePlot.get_voltage_scale()[0])/255 + self.scopePlot.get_voltage_scale()[0]

            # Add data to the temporary buffer
            list(channelBuffers.values())[x].extend(scaledData)

            # Go to next channel in sequence
            currentChannel = next(channelSequence)
            
            # Make sure next byte is a zero (if it isn't, resync)
            if (self._next_int() != 0):
                self.resync()

        # Finally, return device data for further processing
        return channelBuffers

    def resync(self):
        """Get data from input buffer until we hit a null terminator"""

        # Reset data to keep it aligned
        self.scopePlot.clear_channel_buffers()

        # Search for the next null (up to a limit)
        limit = OscilloscopeView.BLOCK_SIZE * len(self.channelLabels) * OscilloscopeView.RESYNC_ATTEMPTS
        while (self._next_int() != 0):
            limit -= 1

            if limit <= 0:
                raise IOError("Unable to resync with device.")

    def clear_buffers(self):
        self.scopePlot.clear_channel_buffers()
        self.device.reset_input_buffer()
        self.device.reset_output_buffer()

    def _is_connected(self):
        if (self.device != None):
            return self.device.is_open

        return False

    def _next_channel_block_start(self):
        nextChannelNum = self._next_int()

        if (nextChannelNum >= 1 and nextChannelNum <= len(self.channelLabels)):
            return nextChannelNum
        
        return -1

    def _next_int(self, byteCount=1):
        try:
            return int.from_bytes(self.device.read(byteCount), byteorder='big', signed=False)
        except Exception:
            raise IOError("Device disconnected")

# For properly handling Qt's rubbish signals and slots reconnections
def reconnect(signal, newhandler=None, oldhandler=None):
    while True:
        try:
            if oldhandler is not None:
                signal.disconnect(oldhandler)
            else:
                signal.disconnect()
        except TypeError:
            break
    if newhandler is not None:
        signal.connect(newhandler)

# Just for the terminal
def rpath(*relative_to_working):
    return path.join(path.realpath(path.dirname(__file__)), path.join(*relative_to_working))

def main():
    app = QtGui.QApplication(sys.argv)

    app_icon = QtGui.QIcon()
    app_icon.addFile(rpath('resources', 'logo', 'logo_small.png'), QtCore.QSize(16,16))
    app_icon.addFile(rpath('resources', 'logo', 'logo_small.png'), QtCore.QSize(24,24))
    app_icon.addFile(rpath('resources', 'logo', 'logo_small.png'), QtCore.QSize(32,32))
    app_icon.addFile(rpath('resources', 'logo', 'logo_small.png'), QtCore.QSize(48,48))
    app_icon.addFile(rpath('resources', 'logo', 'logo_small.png'), QtCore.QSize(256,256))
    app.setWindowIcon(app_icon)

    gui = OscilloscopeView()
    app.exec_()

if __name__ == '__main__':
    #cProfile.run("main()")
    main()