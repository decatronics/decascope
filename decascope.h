/* 

Decascope

Simple two-channel oscilloscope designed for Arduino.

Author: Jason Storey
*/

#ifndef _DECASCOPE_H_
#define _DECASCOPE_H_

#include <Arduino.h>
#include "stdint.h"

// Defines for setting and clearing register bits
// (Utility functions)
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

// Versioning
#define VERSION_MAJ 1
#define VERSION_MIN 0

#define BLOCK_SIZE 600 // Number of data points to send at once
#define TRANSMIT_HEADSTART 5 // Pre-start transmission before buffer wraps around

// As fast as it gets without reliability issues
#define BAUDRATE 230400

#define CHANNEL_COUNT 2
#define CHANNEL_1 A0
#define CHANNEL_2 A1
// Number to send as a flag for the channel number 
#define CHANNEL_1_INDEX 1 // Use 1-based indexing because 0 is reserved for frame sync
#define CHANNEL_2_INDEX 2
#define PWM_OUT 12

// Buffers for each conversion result sweep
volatile uint8_t channel1_buffer[BLOCK_SIZE];
volatile uint8_t channel1_shadow[BLOCK_SIZE];
volatile uint8_t channel2_buffer[BLOCK_SIZE];
volatile uint8_t channel2_shadow[BLOCK_SIZE];

// For keeping track of where we're at in the active buffer
volatile uint16_t channel1_head = 0;
volatile uint16_t channel2_head = 0;

// Indicates when buffers are full and need to be transmitted
// (Transmission time must be faster than fill time)
volatile bool start_transmit = false;

// Indicates the active buffer
volatile bool shadow_buffers = false;

// Indicates which channel we're reading into currently
volatile uint8_t reading_channel = 1;

#endif