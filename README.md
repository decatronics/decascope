# Decascope v1.0

> Author: Jason Storey

> All rights reserved

Decascope is a simple Arduino oscilloscope. 

Visit [the main website](https://decatronics.gitlab.io/decascope) for more info.